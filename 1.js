class Robot{
    constructor(name){
        this.name=name;
    }
    move(){
        console.log(`${this.name} is moving!`)
    }
}
let r0=new Robot('Luuvy');
r0.move();

class Weapon{
    constructor(description){
        this.description=description;
    }
    fire(){
        console.log(`${this.description} is firing!`);
    }
}
let w0=new Weapon('gun');
w0.fire();
 
class CombatRobot extends Robot{
    constructor(name){
        super(name);
        this.weapons=[];
    }
    addWeapon(w){
        this.weapons.push(w);
    }
    fire(){
        for(let w of this.weapons){
            w.fire();
        }
    }
}

let r1=new CombatRobot('a combat robot');
r1.addWeapon(w0);
r1.fire();

Robot.prototype.fly=function(){
    console.log(`${this.name} is flying`);
}
r1.fly();

let f0=r1.fly
f0();
f0.apply(r1,[]);
f0.call(r0);
