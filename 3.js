//module
const doStuff=()=>{
    console.log('I am doing module stuff');
}
class Robot{
    constructor(name){
        this.name=name;
    }
    move(){
        console.log(`${this.name} is moving`);
    }
}
module.exports={
    doStuff,
    Robot
}