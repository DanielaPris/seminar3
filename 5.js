function genCheckPrime(){
    let cache=[2,3];
    let checkAgainstCache=(n)=>{
        for(let e of cache){
            if(!(n%e)){
                return false;
            }
        }
        return true;
    }
    return (n)=>{
        let found=cache.indexOf(n)!==-1;
        if(found){
            return true;
        }
        else{
            for(let i=cache[cache.length-1]+1;i<=Math.sqrt(n);i++){
                if(checkAgainstCache(i)){
                    cache.push(i);
                }
            }
            console.log('CACHE');
            console.log(cache);
            return checkAgainstCache(n);
        }
    }
}
let checkPrime=genCheckPrime()
console.log(checkPrime(120))
console.log(checkPrime(225))
