// function format(s, formatSpec){
//     let modified=s;
//     for(let prop in formatSpec){
//         if(s.indexOf("{"+prop+"}")!==1){
//             modified=modified.replace("{"+prop+"}",formatSpec[prop]);
//         }
//     }
//     return modified;
// }
// console.log(format("I am {name} and I am  a  {role}",{name:"Jimin", role:"singer"}));

String.prototype.format=function(formatSpec){
    let modified=this;
    for(let prop in formatSpec){
        if(this.indexOf("{"+prop+"}")!==1){
            modified=modified.replace("{"+prop+"}",formatSpec[prop]);
        }
    }
    return modified;
}
console.log("I am {name} and I am  a  {role}".format({name:"Jimin", role:"singer"}))
